package me.inscription.trendy;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Xml;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Telrin on 06.05.2015.
 */

public class DataReceiverTask extends AsyncTask<Void, Void, Void> {
    public String hottrendsResponse = "";

    TableTrendy dbHelper = new TableTrendy(TrendyView.context);
    SQLiteDatabase sdb;

    public void dbOpen() {
        sdb = dbHelper.getWritableDatabase();
    }

    public void dbClear() {
        sdb.execSQL(TableTrendy.DATABASE_DELETE_SCRIPT);
        sdb.execSQL(TableTrendy.DATABASE_CREATE_SCRIPT);
    }

    public void dbClose() {
        dbHelper.close();
    }

    public void receivedStringProcessing() throws XmlPullParserException, IOException {
        if (!hottrendsResponse.isEmpty()) {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            InputStream stream = new ByteArrayInputStream(hottrendsResponse.getBytes());
            parser.setInput(stream, null);
            String tag = "";
            String text = "";

            String title = "";
            String htPicture = "";
            String htApprox_traffic = "";
            String htNews_item_title = "";
            String htNews_item_snippet = "";
            String htNews_item_source = "";
            String htNews_item_url = "";

            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (parser.getEventType()) {
                    case XmlPullParser.START_TAG:
                        tag = parser.getName();
                        break;
                    case XmlPullParser.END_TAG:
                        if (parser.getName().equals("item")) {
                            // All Item filled
//                            items.add(new Item( title,
//                                                htPicture,
//                                                htApprox_traffic,
//                                                htNews_item_title,
//                                                htNews_item_snippet,
//                                                htNews_item_source,
//                                                htNews_item_url));
                            new Item(title,
                                    htPicture,
                                    htApprox_traffic,
                                    htNews_item_title,
                                    htNews_item_snippet,
                                    htNews_item_source,
                                    htNews_item_url).dbAddItem(sdb);
                            //
                            title = "";
                            htPicture = "";
                            htApprox_traffic = "";
                            htNews_item_title = "";
                            htNews_item_snippet = "";
                            htNews_item_source = "";
                            htNews_item_url = "";
                        }
                        tag = "";
                        break;
                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        if (tag.equals("title")) {
                            title = text;
                        } else if (tag.equals("ht:picture")) {
                            htPicture = text;
                        } else if (tag.equals("ht:approx_traffic")) {
                            htApprox_traffic = text;
                        } else if (tag.equals("ht:news_item_title")) {
                            htNews_item_title = text;
                        } else if (tag.equals("ht:news_item_snippet")) {
                            htNews_item_snippet = text;
                        } else if (tag.equals("ht:news_item_source")) {
                            htNews_item_source = text;
                        } else if (tag.equals("ht:news_item_url")) {
                            htNews_item_url = text;
                        }
                        break;
                    default:
                        break;
                }
                parser.next();
            }
            //
        }

    }

//    public void dbAddItems(){
//        int listSize = items.size();
//        for (int i = 0; i < listSize; i++){
//            items.get(i).dbAddItem(sdb);
//        }
//    }

    @Override
    protected Void doInBackground(Void... params) {
        hottrendsResponse = "";
        String query = "http://www.google.com/trends/hottrends/atom/feed?pn=p" +
                String.valueOf(TrendyView.countryCode);
        do {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(query);
            try {
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity httpEntity = response.getEntity();
                hottrendsResponse = EntityUtils.toString(httpEntity, HTTP.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (hottrendsResponse.isEmpty());
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (hottrendsResponse!=null) {
            dbOpen();
            dbClear();
            try {
                receivedStringProcessing();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            TrendyView.items = getItems();
            TrendyView.firstFillButtons();
            //        dbAddItems();
            dbClose();
            TrendyView.showButtons(TrendyView.activity);
        }
        super.onPostExecute(result);
    }

    public List<Item> getItems() {
        List<Item> items = new ArrayList<>();
        String title = "";
        String htPicture = "";
        String htApprox_traffic = "";
        String htNews_item_title = "";
        String htNews_item_snippet = "";
        String htNews_item_source = "";
        String htNews_item_url = "";

        Cursor cursor = sdb.query(TableTrendy.TABLE_NAME, new String[]{
                        TableTrendy.COL_TITLE,
                        TableTrendy.COL_HT_PICTURE,
                        TableTrendy.COL_HT_APPBOX_TRAFFIC,
                        TableTrendy.COL_HT_NEWS_ITEM_TITLE,
                        TableTrendy.COL_HT_NEWS_ITEM_SNIPPET,
                        TableTrendy.COL_HT_NEWS_ITEM_SOURCE,
                        TableTrendy.COL_HT_NEWS_ITEM_URL},
                null, null,
                null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                title = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_TITLE));
                htPicture = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_PICTURE));
                htApprox_traffic = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_APPBOX_TRAFFIC));
                htNews_item_title = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_NEWS_ITEM_TITLE));
                htNews_item_snippet = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_NEWS_ITEM_SNIPPET));
                htNews_item_source = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_NEWS_ITEM_SOURCE));
                htNews_item_url = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_NEWS_ITEM_URL));
                items.add(new Item(title,
                        htPicture,
                        htApprox_traffic,
                        htNews_item_title,
                        htNews_item_snippet,
                        htNews_item_source,
                        htNews_item_url));
                cursor.moveToNext();
            }
            while (!cursor.isLast());
            title = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_TITLE));
            htPicture = String.valueOf(TrendyView.rnd.nextInt(8));//cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_PICTURE));
            htApprox_traffic = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_APPBOX_TRAFFIC));
            htNews_item_title = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_NEWS_ITEM_TITLE));
            htNews_item_snippet = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_NEWS_ITEM_SNIPPET));
            htNews_item_source = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_NEWS_ITEM_SOURCE));
            htNews_item_url = cursor.getString(cursor.getColumnIndex(TableTrendy.COL_HT_NEWS_ITEM_URL));
            items.add(new Item(title,
                    htPicture,
                    htApprox_traffic,
                    htNews_item_title,
                    htNews_item_snippet,
                    htNews_item_source,
                    htNews_item_url));
        }
        cursor.close();
        return items;
    }
}

