package me.inscription.trendy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

/**
 * Created by dvp on 08.05.2015.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView imgView;
    String imgURL;

    public DownloadImageTask(ImageView imgView, String imgURL) {
        this.imgView = imgView;
        this.imgURL = "http:" + imgURL;
    }

    protected void onPostExecute(Bitmap result) {
        if (result!=null){
            imgView.setImageBitmap(result);
            imgView.clearAnimation();
            imgView.animate().cancel();
        }
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        Bitmap bmp = null;
        do {
            try {
                InputStream in = new java.net.URL(imgURL).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        } while (bmp == null);
        return  bmp;
    }
}