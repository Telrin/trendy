package me.inscription.trendy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class InternetStateReseiver extends BroadcastReceiver {
    public InternetStateReseiver() {
    }

    public static boolean isInternetActive(Context context) {
        final ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) return true;
        return false;
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if (intent.getAction().equalsIgnoreCase("android.intent.action.AIRPLANE_MODE")) {
            String message = intent.getAction();

            Toast.makeText(context, message,
                    Toast.LENGTH_LONG).show();
        }
    }
}
