package me.inscription.trendy;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by dvp on 07.05.2015.
 */
public class Item {
    public final String title;
    public final String htPicture;
    public final String htApprox_traffic;
    public final String htNews_item_title;
    public final String htNews_item_snippet;
    public final String htNews_item_source;
    public final String htNews_item_url;

    public Item(String title,
                String htPicture,
                String htApprox_traffic,
                String htNews_item_title,
                String htNews_item_snippet,
                String htNews_item_source,
                String htNews_item_url) {
        this.title = title;
        this.htPicture = htPicture;
        this.htApprox_traffic = htApprox_traffic;
        this.htNews_item_title = htNews_item_title;
        this.htNews_item_snippet = htNews_item_snippet;
        this.htNews_item_source = htNews_item_source;
        this.htNews_item_url = htNews_item_url;
    }
    public void dbAddItem(SQLiteDatabase sdb){
        ContentValues newValues = new ContentValues();
        newValues.put(TableTrendy.COL_TITLE, title);
        newValues.put(TableTrendy.COL_HT_PICTURE, htPicture);
        newValues.put(TableTrendy.COL_HT_APPBOX_TRAFFIC, htApprox_traffic);
        newValues.put(TableTrendy.COL_HT_NEWS_ITEM_TITLE, htNews_item_title);
        newValues.put(TableTrendy.COL_HT_NEWS_ITEM_SNIPPET, htNews_item_snippet);
        newValues.put(TableTrendy.COL_HT_NEWS_ITEM_SOURCE, htNews_item_source);
        newValues.put(TableTrendy.COL_HT_NEWS_ITEM_URL, htNews_item_url);
        sdb.insert(TableTrendy.TABLE_NAME, null, newValues);
    }
}
