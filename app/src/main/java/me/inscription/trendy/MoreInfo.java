package me.inscription.trendy;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class MoreInfo extends ActionBarActivity {
    private static final String EXTRA_IMAGE = "EXTRA_IMAGE";
//    final Animation animationRotateCenter = AnimationUtils.loadAnimation(
//            this, R.anim.rotate_center);
    int bg = 5;
    String openURL = "";
    Bitmap imgToDraw = null;

    public void onOpenURL(View view) {
        Intent intentOpenURL = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(openURL));
        startActivity(intentOpenURL);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        String title = getIntent().getExtras().getString("title");
        String link = getIntent().getExtras().getString("link");
        String source = getIntent().getExtras().getString("source");
        String infoTitle = getIntent().getExtras().getString("infoTitle");
        String info = getIntent().getExtras().getString("info");
        String count = getIntent().getExtras().getString("count");
        String bg = getIntent().getExtras().getString("bg");
        String pic = getIntent().getExtras().getString("pic");
        int resID = getResources().getIdentifier(bg, "drawable", "me.inscription.trendy");
        View v = findViewById(R.id.bgMoreInfo);
        Bitmap bmp = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                getResources(), resID), 100, 100, true);
        Drawable d = new BitmapDrawable(bmp);
        v.setBackgroundDrawable(d.getCurrent());
        TextView tvTitle = (TextView)findViewById(R.id.tvTitle);
        TextView tvLink = (TextView)findViewById(R.id.tvLink);
        TextView tvInfoPre = (TextView)findViewById(R.id.tvInfoPre);
        TextView tvInfoMain = (TextView)findViewById(R.id.tvInfoMain);
        TextView tvCount = (TextView)findViewById(R.id.tvCount);
        ImageView imgView = (ImageView) findViewById(R.id.imgView);

        tvTitle.setText(Html.fromHtml(title));
        tvLink.setText(Html.fromHtml(source));
        tvInfoPre.setText(Html.fromHtml(infoTitle));
        tvInfoMain.setText(Html.fromHtml(info));
        tvCount.setText(Html.fromHtml(count));
        if (imgToDraw==null) {
            DownloadImageTask downloadImageTask = new DownloadImageTask(imgView, pic);
            if (InternetStateReseiver.isInternetActive(TrendyView.context)) downloadImageTask.execute();
        }
        else {
            imgView.setBackgroundDrawable(new BitmapDrawable(imgToDraw));
//            imgView.setImageDrawable(new BitmapDrawable(imgToDraw));
        }
        openURL = link;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_info);
        if (imgToDraw == null) {
            ImageView imgView = (ImageView) findViewById(R.id.imgView);
            //Anim
            imgView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_center));
            //Anim
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_more_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (imgToDraw != null){
            ImageView imgView = (ImageView) findViewById(R.id.imgView);
            imgToDraw = drawableToBitmap(imgView.getDrawable().getCurrent());
            outState.putParcelable(EXTRA_IMAGE, imgToDraw);
        }
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle inState) {
        super.onRestoreInstanceState(inState);
        imgToDraw = inState.getParcelable(EXTRA_IMAGE);
    }


}
