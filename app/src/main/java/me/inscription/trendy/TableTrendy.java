package me.inscription.trendy;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
//extends SQLiteOpenHelper

public class TableTrendy extends SQLiteOpenHelper implements BaseColumns {
    public static final String DB_NAME = "trendy.db";
    public static final int DB_VER = 2;

    public static final String TABLE_NAME = "trendy";

    public static final String COL_TITLE = "title";
    public static final String COL_HT_PICTURE = "htPicture";
    public static final String COL_HT_APPBOX_TRAFFIC = "htApprox_traffic";
    public static final String COL_HT_NEWS_ITEM_TITLE = "htNews_item_title";
    public static final String COL_HT_NEWS_ITEM_SNIPPET = "htNews_item_snippet";
    public static final String COL_HT_NEWS_ITEM_SOURCE = "htNews_item_source";
    public static final String COL_HT_NEWS_ITEM_URL = "htNews_item_url";

//    public static SQLiteDatabase db;


//    public TableTrendy(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
//        super(context, name, factory, version);
////        db = getWritableDatabase();
////        clearDB(context, name, version);
//    }

//    public void clearDB(Context context, String dbName, int dbVer){
//        db.delete(TableTrendy.TABLE_NAME, null, null);
//        String str = "";
//    }

    public static final String DATABASE_CREATE_SCRIPT = "CREATE TABLE "
            + TABLE_NAME + "("
            + BaseColumns._ID + " integer primary key autoincrement, "
            + COL_TITLE + " text, "
            + COL_HT_PICTURE + " text, "
            + COL_HT_APPBOX_TRAFFIC + " text, "
            + COL_HT_NEWS_ITEM_TITLE + " text, "
            + COL_HT_NEWS_ITEM_SNIPPET + " text, "
            + COL_HT_NEWS_ITEM_SOURCE + " text, "
            + COL_HT_NEWS_ITEM_URL + " text);";

    public static final String DATABASE_DELETE_SCRIPT = "DROP TABLE " + TABLE_NAME;

    public TableTrendy(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    public TableTrendy(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public TableTrendy(Context context, String name, SQLiteDatabase.CursorFactory factory,
                          int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_SCRIPT);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DATABASE_DELETE_SCRIPT);
        onCreate(db);
    }
}
