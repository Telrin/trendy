package me.inscription.trendy;

import android.os.CountDownTimer;

/**
 * Created by Telrin on 11.05.2015.
 */
public class TimeBroadcastReceiver extends CountDownTimer {
    public TimeBroadcastReceiver(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    public void onTick(long millisUntilFinished) {
    }

    private void doSomething() {
        if (!TrendyView.items.isEmpty()) TrendyView.fillNewButton();
    }

    public void onFinish() {
        doSomething();
        start();
    }
}