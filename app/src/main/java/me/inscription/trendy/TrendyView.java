package me.inscription.trendy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class TrendyView extends ActionBarActivity {
    private static final String APP_PREFERENCES = "APP_PREFERENCES";
    private static final String APP_PREF_ITEM_SELECTED = "APP_PREF_ITEM_SELECTED";
    private static final String APP_PREF_COUNTRY_CODE = "APP_PREF_COUNTRY_CODE";
    private TimeBroadcastReceiver timeBroadCastReceiver;
    static int currentlyAnimatedBtn = -1;
    private static final String EXTRA_TRENDS_RESPONSE = "EXTRA_TRENDS_RESPONSE";
    private static final String EXTRA_BUTTON_TITLES = "EXTRA_BUTTON_TITLES";
    private static final String EXTRA_BUTTON_BACKGROUNDS = "EXTRA_BUTTON_BACKGROUNDS";
    public static Context context;
    public static Activity activity;
    private Drawer.Result drawerResult = null;
    private static DataReceiverTask dataReceiverTask;
    private static DownloadImageTask downloadImageTask;
    public static List<Item> items = new ArrayList<>();
    static List<Button> btnList = new ArrayList<>();
    static Random rnd = new Random();
    public static int itemSelected = 15;
    public static int countryCode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        activity = this;
        setContentView(R.layout.activity_trendy_view);

        if (timeBroadCastReceiver == null){
            timeBroadCastReceiver = new TimeBroadcastReceiver(500, 500);
        }

        btnList.clear();
        btnList.add((Button)findViewById(R.id.button1));
        btnList.add((Button)findViewById(R.id.button2));
        btnList.add((Button)findViewById(R.id.button3));
        btnList.add((Button)findViewById(R.id.button4));
        btnList.add((Button)findViewById(R.id.button5));
        btnList.add((Button)findViewById(R.id.button6));
        Configuration config = getResources().getConfiguration();
        if((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_XLARGE)
        {
            //xlarge screen
            btnList.add((Button)findViewById(R.id.button7));
            btnList.add((Button)findViewById(R.id.button8));
        }

        dataReceiverTask = new DataReceiverTask();
//
        loadPref();
//
        timeBroadCastReceiver.start();
//
        // >> Handle Toolbar
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawerResult = new Drawer()
                .withActivity(this)
                .withToolbar(toolbar)
                //.withActionBarDrawerToggle(true)
                .withHeader(R.layout.drawer_header)
                .addDrawerItems(
//                        new PrimaryDrawerItem().withName("World").withIdentifier(0),
                        new PrimaryDrawerItem().withName("South Africa (.ZA)").withIdentifier(40),
                        new PrimaryDrawerItem().withName("Germany (.DE)").withIdentifier(15),
                        new PrimaryDrawerItem().withName("Saudi Arabia (.SA)").withIdentifier(36),
                        new PrimaryDrawerItem().withName("Argentina (.AR)").withIdentifier(30),
                        new PrimaryDrawerItem().withName("Australia (.AU)").withIdentifier(8),
                        new PrimaryDrawerItem().withName("Austria (.AT)").withIdentifier(44),
                        new PrimaryDrawerItem().withName("Belgium (.BE)").withIdentifier(41),
                        new PrimaryDrawerItem().withName("Brazil (.BR)").withIdentifier(18),
                        new PrimaryDrawerItem().withName("Canada (.CA)").withIdentifier(13),
                        new PrimaryDrawerItem().withName("Chile (.CL)").withIdentifier(38),
                        new PrimaryDrawerItem().withName("Colombia (.CO)").withIdentifier(32),
                        new PrimaryDrawerItem().withName("South Corea (.KR)").withIdentifier(23),
                        new PrimaryDrawerItem().withName("Denmark (.DK)").withIdentifier(49),
                        new PrimaryDrawerItem().withName("Egypt (.EG)").withIdentifier(29),
                        new PrimaryDrawerItem().withName("Spain (.ES)").withIdentifier(26),
                        new PrimaryDrawerItem().withName("United State (.US)").withIdentifier(1),
                        new PrimaryDrawerItem().withName("Finland (.FI)").withIdentifier(50),
                        new PrimaryDrawerItem().withName("France (.FR)").withIdentifier(16),
                        new PrimaryDrawerItem().withName("Greece (.GR)").withIdentifier(48),
                        new PrimaryDrawerItem().withName("Hong Kong (.HK)").withIdentifier(10),
                        new PrimaryDrawerItem().withName("Hungary (.HU)").withIdentifier(45),
                        new PrimaryDrawerItem().withName("India (.IN)").withIdentifier(3),
                        new PrimaryDrawerItem().withName("Indonesia (.ID)").withIdentifier(19),
                        new PrimaryDrawerItem().withName("Israel (.IL)").withIdentifier(6),
                        new PrimaryDrawerItem().withName("Italy (.IT)").withIdentifier(27),
                        new PrimaryDrawerItem().withName("Japan (.JP)").withIdentifier(4),
                        new PrimaryDrawerItem().withName("Kenya (.KE)").withIdentifier(37),
                        new PrimaryDrawerItem().withName("Malaysia (.MY)").withIdentifier(34),
                        new PrimaryDrawerItem().withName("Mexico (.MX)").withIdentifier(21),
                        new PrimaryDrawerItem().withName("Nigeria (.NG)").withIdentifier(52),
                        new PrimaryDrawerItem().withName("Norway (.NO)").withIdentifier(51),
                        new PrimaryDrawerItem().withName("Netherlands (.NL)").withIdentifier(17),
                        new PrimaryDrawerItem().withName("Phillipines (.PH)").withIdentifier(25),
                        new PrimaryDrawerItem().withName("Poland (.PL)").withIdentifier(31),
                        new PrimaryDrawerItem().withName("Portugal (.PT)").withIdentifier(47),
                        new PrimaryDrawerItem().withName("Czech Republic (.CZ)").withIdentifier(43),
                        new PrimaryDrawerItem().withName("Romania (.RO)").withIdentifier(39),
                        new PrimaryDrawerItem().withName("United Kingdom (.GB)").withIdentifier(9),
                        new PrimaryDrawerItem().withName("Russia (.RU)").withIdentifier(14),
                        new PrimaryDrawerItem().withName("Singapore (.SG)").withIdentifier(5),
                        new PrimaryDrawerItem().withName("Sweden (.SE)").withIdentifier(42),
                        new PrimaryDrawerItem().withName("Switzerland (.CH)").withIdentifier(46),
                        new PrimaryDrawerItem().withName("Taiwan (.TW)").withIdentifier(12),
                        new PrimaryDrawerItem().withName("Thailand (.TH)").withIdentifier(33),
                        new PrimaryDrawerItem().withName("Turkey (.TR)").withIdentifier(24),
                        new PrimaryDrawerItem().withName("Ukraine (.UA)").withIdentifier(35),
                        new PrimaryDrawerItem().withName("Vietnam (.VN)").withIdentifier(28)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                       @Override
                       public void onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                           if (countryCode!=iDrawerItem.getIdentifier()){
                               itemSelected = i;
                               countryCode=iDrawerItem.getIdentifier();
                               dataReceiverTask.cancel(true);
                               dataReceiverTask = new DataReceiverTask();
                               savePref();
                           }
                           reload();
                       }
                   }
                )
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        InputMethodManager inputMethodManager = (InputMethodManager) TrendyView.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(TrendyView.this.getCurrentFocus().getWindowToken(), 0);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }
                })
                .withSelectedItem(itemSelected)
                .build();
        // << Handle Toolbar
    }

    private void savePref() {
        SharedPreferences sharedPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(APP_PREF_ITEM_SELECTED, itemSelected-1);
        editor.putInt(APP_PREF_COUNTRY_CODE, countryCode);
        editor.apply();
    }

    private void loadPref(){
        SharedPreferences sharedPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(APP_PREF_ITEM_SELECTED)) {
            itemSelected=sharedPreferences.getInt(APP_PREF_ITEM_SELECTED, 0);
        }
        if (sharedPreferences.contains(APP_PREF_COUNTRY_CODE)) {
            countryCode=sharedPreferences.getInt(APP_PREF_COUNTRY_CODE, 0);
        }
    }

    public void hideButtons(Activity activity){
        ImageView iv = (ImageView) activity.findViewById(R.id.imageView);
        TableLayout t = (TableLayout) activity.findViewById(R.id.tableLayout);
        //Anim
        iv.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.rotate_center));
        //Anim
        iv.setVisibility(View.VISIBLE);
        t.setVisibility(View.GONE);
    }

    public static void showButtons(Activity activity){
        ImageView iv = (ImageView) activity.findViewById(R.id.imageView);
        TableLayout t = (TableLayout) activity.findViewById(R.id.tableLayout);
        iv.setVisibility(View.GONE);
        //Anim
        iv.clearAnimation();
        iv.animate().cancel();
        //Anim

        t.setVisibility(View.VISIBLE);
    }

    public void reload() {
        if (dataReceiverTask.hottrendsResponse.isEmpty()) {
            if (InternetStateReseiver.isInternetActive(context)) dataReceiverTask.execute();
            hideButtons(activity);
        }
        else if (items.isEmpty()){
            dataReceiverTask.dbOpen();
            items = dataReceiverTask.getItems();
            firstFillButtons();
            dataReceiverTask.dbClose();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        reload();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_trendy_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // Закрываем Navigation Drawer по нажатию системной кнопки "Назад" если он открыт
        if (drawerResult.isDrawerOpen()) {
            drawerResult.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(EXTRA_TRENDS_RESPONSE, dataReceiverTask.hottrendsResponse);
        if (!dataReceiverTask.hottrendsResponse.isEmpty()){
            List<String> usedButtonsTitle = new ArrayList<>();
            List<String> usedButtonBg = new ArrayList<>();
            for (int i =0; i < btnList.size(); i++){
                usedButtonsTitle.add(btnList.get(i).getText().toString());
                usedButtonBg.add(btnList.get(i).getTag().toString());
            }
            outState.putStringArrayList(EXTRA_BUTTON_TITLES, (ArrayList<String>) usedButtonsTitle);
            outState.putStringArrayList(EXTRA_BUTTON_BACKGROUNDS, (ArrayList<String>) usedButtonBg);
        }
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        dataReceiverTask.hottrendsResponse = savedInstanceState.getCharSequence(EXTRA_TRENDS_RESPONSE).toString();
        if (!dataReceiverTask.hottrendsResponse.isEmpty()){
            List<String> usedButtonTitle = new ArrayList<>();
            List<String> usedButtonBg = new ArrayList<>();
            usedButtonTitle = savedInstanceState.getStringArrayList(EXTRA_BUTTON_TITLES);
            usedButtonBg = savedInstanceState.getStringArrayList(EXTRA_BUTTON_BACKGROUNDS);
            Button btn;
            for (int i =0; i < btnList.size(); i++){
                btn = btnList.get(i);
                btn.setText(usedButtonTitle.get(i));
                btn.setTag(usedButtonBg.get(i));
                int resID = context.getResources().getIdentifier(usedButtonBg.get(i), "drawable", "me.inscription.trendy");
                Bitmap bmp = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                        context.getResources(), resID), 100, 100, true);
                Drawable d = new BitmapDrawable(bmp);
                btn.setBackgroundDrawable(d.getCurrent());
            }
        }
    }

    @Override
    protected void onDestroy() {
        dataReceiverTask.dbClose();
        timeBroadCastReceiver.cancel();
        super.onDestroy();
    }

//    public static Drawable webLoadImage(String urlStr) {
//        URL url = null;
//        Bitmap bmp = null;
//        try {
//            url = new URL("http:" + urlStr);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        try {
//            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Drawable res = null;
//        if (bmp!=null){res = new BitmapDrawable(context.getResources(), bmp);}
//        return res;
//    }
    public static void fillNewButton() {
        int k;
        Button btn;
        String str;
        int i = 0;
        int intBtnToChange;
        do{
            intBtnToChange = rnd.nextInt(btnList.size());
        } while (currentlyAnimatedBtn == intBtnToChange);
        Button btnToChange = btnList.get(intBtnToChange);
        List<String> usedButtons = new ArrayList<>();
        do {
            btn = btnList.get(i);
            usedButtons.add(btn.getText().toString());
            i++;
        } while (i < btnList.size());

        do {
            k = rnd.nextInt(items.size());
        } while ((btnToChange.getText().toString().equals(items.get(k).title)) ||
                (usedButtons.contains(items.get(k).title)));

        //Anim
        btnToChange.startAnimation(AnimationUtils.loadAnimation(context, R.anim.alpha_down));
        currentlyAnimatedBtn = intBtnToChange;
        //Anim

        btnToChange.setText(items.get(k).title);

        int intGenerated = rnd.nextInt(7)+1;
        String resStr = "img_blank_" + String.valueOf(intGenerated);
        btnToChange.setTag(resStr);
        int resID = context.getResources().getIdentifier(resStr, "drawable", "me.inscription.trendy");
        Bitmap bmp = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                context.getResources(), resID), 100, 100, true);
        Drawable d = new BitmapDrawable(bmp);
        btnToChange.setBackgroundDrawable(d.getCurrent());
        //Anim
        btnToChange.startAnimation(AnimationUtils.loadAnimation(context, R.anim.alpha_up));
        //Anim
    }

    public static void firstFillButtons() {
        int k;
        Button btn;
        String str;
        int i = 0;
        List<Integer> usedItems = new ArrayList<>();

        if (!items.isEmpty()){
            do {
                btn = btnList.get(i);
                do { k = rnd.nextInt(items.size());}
                while (usedItems.contains(k));
                usedItems.add(k);
                btn.setText(items.get(k).title);
                str = items.get(k).htPicture;

                int intGenerated = rnd.nextInt(7)+1;
                String resStr = "img_blank_" + String.valueOf(intGenerated);
                btn.setTag(resStr);
                int resID = context.getResources().getIdentifier(resStr, "drawable", "me.inscription.trendy");
                Bitmap bmp = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                        context.getResources(), resID), 100, 100, true);
                Drawable d = new BitmapDrawable(bmp);
                btn.setBackgroundDrawable(d.getCurrent());

                i++;
            }
            while (i < btnList.size());
        }
    }

    public void onClick(View view)
    {
        if (!dataReceiverTask.hottrendsResponse.isEmpty()){
            Intent intent = new Intent(this, MoreInfo.class);
            Button btn = (Button)view;
            String title = btn.getText().toString();
            String link = null;
            String pic = null;
            String source = null;
            String infoTitle = null;
            String info = null;
            String count = null;
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).title.equals(title)) {
                    link = items.get(i).htNews_item_url;
                    infoTitle = items.get(i).htNews_item_title;
                    source = items.get(i).htNews_item_source;
                    info = items.get(i).htNews_item_snippet;
                    count = items.get(i).htApprox_traffic;
                    pic = items.get(i).htPicture;
                }
            }
            String bg = String.valueOf(view.getTag());

            intent.putExtra("title", title);
            intent.putExtra("bg", bg);
            intent.putExtra("pic", pic);
            intent.putExtra("link", link);
            intent.putExtra("source", source);
            intent.putExtra("infoTitle", infoTitle);
            intent.putExtra("info", info);
            intent.putExtra("count", count);

            startActivity(intent);

        }
        else {
            Toast.makeText(context, "FAIL", Toast.LENGTH_LONG).show();
        }

    }
}
